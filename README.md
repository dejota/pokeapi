# pokeapi challenge

## ⛏️ Built Using

- [VueJS](https://vuejs.org/) - The Progressive JavaScript Framework
- [Vuetify](https://vuetifyjs.com/en/) - Vuetify is a Vue UI Library with beautifully handcrafted Material Components
- [Node.js](https://nodejs.org/) - JavaScript engine
- [vue-axios](https://github.com/imcvampire/vue-axios) - A small wrapper for integrating axios to Vuejs

---

## 🏁 Getting Started

### Prerequisites:

Install **Git**: [https://git-scm.com/book/en/v2/Getting-Started-Installing-Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

Install **Node**: [https://nodejs.org/](https://nodejs.org/)

---

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

---

## Usage

```
Home section:

  Search: you can search for a specific Pokemon. The search button is available only when the user type something. Also if the user use the Enter key. This action redirects to Result Section.

  Cards: The home page will show to the user 20 pokemons. Each Pokemon with his picture, name, number, type and stats.

  Get next 20 Pokemons: When the user clicks that button, the home page loads the next 20 pokemons that comes from the api.



Result section:

  If the result of the search is satisfactory, shows the Pokemon in the same way as in the home page.

  Otherwise the message of Pokemon not found appears

  At the end of both results it's a Back to Home link.



404 section:

  If for any reason the user write an incorrect url, vue redirect the user to 404 Page not found with a Back to Home link.


Note: The user can see see the app in desktop, tablet or mobile.

```

---

## ✍️ Author

- [Juan Carlos Quintero](https://www.linkedin.com/in/quinterojuan/)

<br><br>

![picture](./src/assets/pokereadme.jpg)
